Rails.application.routes.draw do
  resources :users, only: [:create, :update, :destroy]
  post 'authenticate', to: 'users#authenticate'
end
