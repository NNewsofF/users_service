class User < ApplicationRecord
  has_secure_password

  validates :first_name, :last_name, :middle_name, presence: true
  validates :phone, presence: true, uniqueness: true, format: { with: /\A\d{10}\z/, message: "должен содержать 10 цифр" }
  validates :bank_card, presence: true, format: { with: /\A\d{16}\z/, message: "должен содержать 16 цифр" }
  validates :birth_date, presence: true
  validates :password, length: { minimum: 6 }
end

