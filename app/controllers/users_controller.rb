class UsersController < ApplicationController
  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    head :no_content
  end

  def authenticate
    @user = User.find_by(phone: params[:phone])
    if @user&.authenticate(params[:password])
      render json: @user
    else
      render json: { error: 'Invalid credentials' }, status: :unauthorized
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :middle_name, :birth_date, :phone, :bank_card, :password, :password_confirmation)
  end
end

